class HomesController < ApplicationController
  def index
    @response = ['OK', {
      messages: 'Hello, welcome to binar blog batch #10'
    }, nil]
    render json: {
      status: :ok, result: @response.second, errors: @response.third
    }
  end
end
